from display import display
from pathlib import Path
import json
fpath = Path(__file__).parent


class window(display):
    def __init__(self):
        super().__init__()
        self.load_setup_json()

    def print_a(self):
        print(self.arrow + "> ", end='')

    def load_setup_json(self):
        with open(fpath/'transpy_setup.json', 'r', encoding='utf-8') as f:
            self.setup_content = json.load(f)

    def load_language_lib_json(self):
        with open(fpath/'transpy_language_lib.json', 'r', encoding='utf-8') as f:
            return json.load(f)

    def guess_instruction(self, word, instruction_list):
        instruction_set_list = map(
            lambda instruction: set(instruction), instruction_list)
        instruction_len_list = list(map(lambda instruction_set: len(
            set(word) & instruction_set), instruction_set_list))
        print(self.error + " 想输入" +
              instruction_list[instruction_len_list.index(max(instruction_len_list))]+"？ ")
