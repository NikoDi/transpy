# transpy

Lucy Edition
#### 介绍

一个用Python开发的命令行翻译软件

#### 软件架构

基于百度免费的通用翻译API


#### 安装教程

不用教

#### 使用说明

把软件加入PATH使用更方便  
help在软件里有

#### 现有功能

- 在命令行内翻译
- 两种显示模式
- 将翻译结果写入粘贴板

#### 开发计划

- 历史记录  
- 整理文件和代码

#### 编译说明

本软件使用pyinstaller编译。如果要自己使用源代码编译，请注意，由于直接编译体积较大，请先创建虚拟环境（比如pipenv），再逐个测试使用最小支持库来进行编译，最好同时使用upx。
- 编译使用的pipenv虚拟环境使用的Pipefile和Pipefile.lock文件存放在了compile/pipenv_use文件夹中。
- pyinstaller使用的脚本是transpy.spec，记得修改其中的路径。

软件的安装包使用VNISEdit（NSIS）制作。
- 安装包制作使用的软件VNISEdit（NSIS）和脚本transpy.nsi存放在了compile/make_installer文件夹中。

#### 版本说明

现在软件还在非常早期的版本，发行版是为了方便非开发者的内测用户下载而添加，不代表软件的最终形态。早期版本使用代号而非数字标注版本。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
