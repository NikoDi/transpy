# -*- coding: utf-8 -*-
from requests import post as requests_post
from random import randint
import json
from hashlib import md5
from prettytable import PrettyTable
from pathlib2 import Path
from setup_map import *
from window import window
from nltk import sent_tokenize as nltk_sent_tokenize# 英文分句
from zhon.hanzi import sentence as zhon_hanzi_sentence# 中文分句
from langid import classify as langid_classify# 语种检测
from re import findall as re_findall
from pyperclip import copy as pyperclip_copy
fpath = Path(__file__).parent


class translate_window(window):
    def __init__(self, f_lang='auto', t_lang='zh'):
        super().__init__()
        self.f_lang = f_lang
        self.t_lang = t_lang

        self.instruction_list = ['--quit']
        print("如果你想返回上一级，请输入"+self.highlight+' --quit ')

        self.run()

    def run(self):
        self.quit = False
        while not self.quit:
            print(self.hint+'('+self.f_lang+'->'+self.t_lang+')', end='')
            self.print_a()
            instruction = input()
            self.judge_instruction(instruction)
        entry_window(is_start=False)

    def judge_instruction(self, instruction):
        words = instruction.split()

        if words == []:
            return
        elif words[0][:2] == '--':
            if words[0] in self.instruction_list:
                self.execute_instruction(words)
            else:
                self.guess_instruction(words[0], self.instruction_list)
        else:
            self.translate(instruction)

    def execute_instruction(self, words):
        if words[0] == self.instruction_list[0]:
            self.quit = True

    def translate(self, query):

        appid = '20191004000339063'
        appkey = 'B3CIsgDUP3rjhEgo45B3'

        endpoint = 'https://api.fanyi.baidu.com'
        path = '/api/trans/vip/translate'
        url = endpoint + path

        query = self.cut_sentence(query)

        # Generate salt and sign

        salt = randint(32768, 65536)
        sign = self.make_md5(appid + query + str(salt) + appkey)

        # Build request
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        payload = {'appid': appid, 'q': query, 'from': self.f_lang,
                   'to': self.t_lang, 'salt': salt, 'sign': sign}

        # Send request
        r = requests_post(url, params=payload, headers=headers)
        self.result = r.json()

        self.display_translation_result()
        if self.setup_content[OUT_CLIPBOARD]['selected'] == 'yes':
            self.write_clipboard()

        # Show response

    def make_md5(self, s, encoding='utf-8'):
        return md5(s.encode(encoding)).hexdigest()

    def cut_sentence(self, input):
        sentences = ''
        if self.f_lang == 'auto':
            detected_lang = self.lang_detect(input)

        if self.f_lang or detected_lang == 'en':
            sentences = "\n".join(nltk_sent_tokenize(input))
        elif self.f_lang or detected_lang == 'zh':
            sentences = sents = '\n'.join(
                re_findall(zhon_hanzi_sentence, input))

        return sentences

    def lang_detect(self, input):
        return langid_classify(input)[0]

    def result_process_mode(self,mode):
        processed_result = ''

        if mode == "1":
            for trans_result in self.result['trans_result']:
                processed_result += '\n' + trans_result["src"] +'\n'+ trans_result['dst']+'\n'
        else:
            for trans_result in self.result['trans_result']:
                processed_result += trans_result["dst"]

        return processed_result

    def write_clipboard(self):
        mode = self.setup_content[OUT_CLIPBOARD_MODE]['selected']
        pyperclip_copy(self.result_process_mode(mode))


    def display_translation_result(self):
        mode = self.setup_content[TRANSLATION_DISPLAY_MODE]['selected']
        print(self.result_process_mode(mode))


class entry_window(window):
    def __init__(self, is_start):
        super().__init__()

        self.instruction_list = ['enable', 'setup', 'quit', 'help', 'showlang']

        if is_start is True and self.setup_content[DISPLAY_HELP]['selected'] == 'yes':
            is_start = False
            self.help()

        self.run()

    def run(self):
        self.quit = False
        while not self.quit:
            self.print_a()
            instruction = input()
            self.judge_instruction(instruction)
        quit()

    def help(self):
        print('· 使用方法：输入' + self.highlight + ' enable [f_lang] [t_lang] ')
        print('· 表示从' + self.highlight + ' f_lang '+self.reset +
              '语言翻译到'+self.highlight + ' t_lang '+self.reset+'语言（默认是简体中文）')
        print('· 例如：' + self.highlight +
              ' enable en zh '+self.reset + '表示从英文翻译成中文')
        print('· 如果输入' + self.highlight +
              ' enable [f_lang] '+self.reset + '则默认为' + self.highlight + ' enable from [f_lang] to zh ')
        print('· 如果输入' + self.highlight + ' enable '+self.reset +
              '则默认为' + self.highlight + ' enable from auto to zh ')
        print("· 常用语言表：")
        self.show_lang()
        print("如果你想对本软件进行一些设置，请输入" + self.highlight + ' setup ')
        print("如果你想查看使用帮助，请输入" + self.highlight + ' help ')
        print("如果你想查看支持的语种和它的代码，请输入" + self.highlight + ' showlang ')
        print("如果你想退出软件，请输入" + self.highlight + ' quit ')

    def show_lang(self):
        table = PrettyTable(['名称1', '代码1', '名称2', '代码2', '名称3', '代码3'])
        language_lib = self.load_language_lib_json()
        language_lib_map = list(language_lib.items())
        language_lib_map = [language_lib_map[i]+language_lib_map[i+1]+language_lib_map[i+2]
                            for i, language in enumerate(language_lib_map) if i % 3 == 0]

        table.add_rows(language_lib_map)
        print(table)

    def judge_instruction(self, instruction):
        words = instruction.split()

        if words == []:
            return
        elif words[0] in self.instruction_list:
            self.execute_instruction(words)
        else:
            self.guess_instruction(words[0], self.instruction_list)

    def execute_instruction(self, words):
        if words[0] == self.instruction_list[0]:
            if len(words) == 1:
                translate_window()
            elif len(words) == 2:
                language_lib = self.load_language_lib_json()
                if words[1] in language_lib.values():
                    translate_window(words[1])
                else:
                    self.guess_instruction(
                        words[1], list(language_lib.values()))
            else:
                language_lib = self.load_language_lib_json()
                if words[1] and words[2] in language_lib.values():
                    if words[2] == 'auto':
                        print(self.error+' t_lang不可以是auto ')
                    else:
                        translate_window(words[1], words[2])
                else:
                    word = words[1] if words[1] not in language_lib.values(
                    ) else words[2]
                    self.guess_instruction(word, list(language_lib.values()))
        elif words[0] == self.instruction_list[1]:
            setup_window()
        elif words[0] == self.instruction_list[2]:
            self.quit = True
        elif words[0] == self.instruction_list[3]:
            self.help()
        elif words[0] == self.instruction_list[4]:
            self.show_lang()


class setup_window(window):
    def __init__(self):
        super().__init__()
        self.instruction_list = ['show', 'quit']

        print('输入' + self.highlight +
              ' [index] [option] ' + self.reset+'对设置进行修改')
        print('输入' + self.highlight + ' show ' + self.reset+'显示当前设置')
        print('输入' + self.highlight + ' quit ' + self.reset+'返回上一级')
        self.show_setup()

        self.run()

    def run(self):
        self.quit = False
        while not self.quit:
            print(self.hint+'(setup)', end='')
            self.print_a()
            instruction = input()
            self.judge_instruction(instruction)
        entry_window(is_start=False)

    def show_setup(self):
        table = PrettyTable(['序号', '功能', '状态', '可选项'])
        table.add_row(['1', '是否每次启动都显示使用帮助？',
                       self.setup_content[DISPLAY_HELP]['selected'], 'yes/no'])
        table.add_row(
            ['2', '翻译是否直接录入粘贴板？', self.setup_content[OUT_CLIPBOARD]['selected'], 'yes/no'])
        table.add_row(['3', '录入粘贴板的内容为对照模式(1)还是专注模式(2)？',
                       self.setup_content[OUT_CLIPBOARD_MODE]['selected'], '1/2'])
        table.add_row(['4', '翻译显示使用对照模式（1）还是专注模式（2）？',
                       self.setup_content[TRANSLATION_DISPLAY_MODE]['selected'], '1/2'])
        print(table)

    def judge_instruction(self, instruction):
        words = instruction.split()
        if words[0] == []:
            return
        elif words[0] in self.instruction_list:
            self.execute_instruction(words)
        elif words[0] not in self.setup_content.keys():
            print(self.error + " 没有这个设置项目 ")
            print(self.error + " 或者 ")
            self.guess_instruction(self.instruction_list)
        elif words[1].lower() not in self.setup_content[words[0]]['selectable']:
            print(self.error + " 该设置项目没有这个选项 ")
        else:
            self.setup_content[words[0]]['selected'] = words[1].lower()
            with open(fpath/'transpy_setup.json', 'w', encoding='utf-8') as f:
                json.dump(self.setup_content, f)

    def execute_instruction(self, words):
        if words[0] == self.instruction_list[0]:
            self.show_setup()
        elif words[0] == self.instruction_list[1]:
            self.quit = True


if __name__ == "__main__":
    print("transpy 露西版")
    entry_window(is_start=True)
