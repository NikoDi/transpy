from colorama import Fore, Back, Style, init
init(autoreset=True)


class display():

    def __init__(self):
        self.error = Back.RED
        self.reset = Style.RESET_ALL
        self.highlight = Back.GREEN + Fore.BLACK
        self.arrow = Fore.CYAN
        self.hint = Fore.RED
